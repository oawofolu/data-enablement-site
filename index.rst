.. Tanzu Data PubSec documentation master file, created by
   sphinx-quickstart on Mon Jun 14 13:29:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tanzu Data (PubSec)
====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   content/overview
   content/deepdives
   content/competitiveintel
   content/relevantlinks


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
