
.. _relevantlinks:

Deep Dives
=======================

.. list-table:: 
   :widths: 50 50
   :header-rows: 1

   * - 
     - Link
   * - Tanzu SQL
     - `Link <#>`_
   * - Data Management for Tanzu
     - `Link <#>`_