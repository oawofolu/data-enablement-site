
.. _relevantlinks:

Relevant External Links
=======================

.. list-table:: 
   :widths: 80 20
   :header-rows: 1


   * -
     - Link 
   * - **Tanzu Data Overview**
     - 
   * - Vault Page
     - `Link <https://vault.vmware.com/group/vault-main-library/data-services1>`_
   * - VMware Site
     - `Link <https://tanzu.vmware.com/data-services>`_
   * - **Tanzu SQL**
     - 
   * - Tanzu SQL Youtube Playlist 
     - `Link <https://www.youtube.com/playlist?list=PLAdzTan_eSPTZHeQVC30iXMsFOfqae_QO>`_
   * - **Tanzu Gemfire**
     - 
   * - Tanzu Gemfire Youtube Playlist 
     - `Link <https://www.youtube.com/playlist?list=PLAdzTan_eSPT4tIq3MzD5Fbs-gxoSgsBY>`_
   * - "Breaking open Apache Geode: How it works and why" (SpringOne Conference) 
     - `Link <https://www.youtube.com/watch?v=qUs3ftvsEoU&list=PL62pIycqXx-QB8JBKEc0Fi6OiUrhLbq6A>`_
   * - **Tanzu Greenplum**
     - 
   * - Greenplum Database
     - `Link <https://www.youtube.com/channel/UCIC2TGO-4xNSAJFCJXlJNwA>`_
   * - **Tanzu RabbitMQ**
     -
   * - Getting started with Tanzu RabbitMQ in 5 minutes 
     - `Link <https://youtu.be/Q3-TJqMch0Q>`_
   * - RabbitMQ YouTube Playlist
     - `Link <https://www.youtube.com/playlist?list=PLAdzTan_eSPTJ2_twF1udWEaEeiSqC5ij>`_
   * - "Thank God it’s Rabbit" 
     - `Link <https://www.youtube.com/channel/UCSg9GRMGAo7euj3baJi4dOg>`_
   * - **Sample Case Studies (Government)**
     -
   * - Preventing $5-6 Billion in refund payments related to fraudulent tax returns
     - `Link <https://tanzu.vmware.com/customers/irs>`_
   * - Using predictive analytics/data mining techniques to improve educational outcomes at Purdue University
     - `Link <https://tanzu.vmware.com/customers/purdue>`_