
Overview
=============

.. contents:: In this section:
   :depth: 1
   :local:

.. _focusareas:

Focus Areas
-----------
- Tanzu SQL
- Tanzu Greenplum
- Tanzu RabbitMQ
- Tanzu Gemfire
- Data Management for Tanzu
- Tanzu Data Transformation
- Tanzu Data Science


.. _recordings:

Recordings
-----------

.. list-table:: 
   :widths: 25 25 25 25
   :header-rows: 1

   * - 
     - Link
     - Link Access Code
     - Slide Deck
   * - Group Enablement (Part  1)
     - `Group Session 1 <https://VMware.zoom.us/rec/share/eg_XeJBQ73ocrMfE1U6dAYpjUK_pndo10lTufzaujQZLPVdRccUaD603VZ6UiRil.KWHHyi3g18S1UNt2>`_ 
     - g*jT1J&U
     - `Slides <https://onevmw-my.sharepoint.com/:p:/g/personal/sedaichamy_vmware_com/EVr2ZNMuO4lGjgvRa2ux9BABOiLMq9zJ2AFW4nhPS4ungg?e=SzLO4h>`_
   * - SE Enablement (Part  1)
     - `SE Session 1 <https://vmware.zoom.us/rec/share/0WyxPjqcBzj92p4j-YuqQQwyk9KN0Hyu41VecoZGGPS8FCj6jPgo3rtaslYH5AYg.DZeZdKCSWqgmL4T0>`_ 
     - F8$58nmf
     - `Slides <https://onevmw-my.sharepoint.com/:p:/g/personal/oawofolu_vmware_com/Ef-7vvj9IA1PpNG33FZTDD0BrQ2mORkloLYgpeYKV8D4IA?e=x97gGc>`_
   * - SE Enablement (Part 2)
     - `SE Session 2 <https://VMware.zoom.us/rec/share/ff_wTu44d630Eajc-a1_kHXkoE6Qslrw6Y1yr5B1XXtk4dcxpK4ZcV6Ujxrk-QOT.IoO9ZW0W8aN-Jwk7>`_
     - n/a
     - n/a
   * - Group Enablement (Part  2)
     - `Group Session 2 <https://VMware.zoom.us/rec/share/09Vru6k3qCsJecXUpAv-tI59Uz0eTlbsTlM8HxEn0N8Z_J2sR6h7DLCt5EhDYMTs.40Rcgq0BGrxUANNx>`_ 
     - n/a
     - `Slides <https://onevmw-my.sharepoint.com/:p:/g/personal/sedaichamy_vmware_com/EZyNKw5o40hBnlRMc9I6CM0BmbumalaH-qdqRiilpwSutQ?e=oHyOcq>`_

.. _whiteboards:

Sample Whiteboards
------------------
.. list-table:: 
   :widths: 20 20 20 20 20
   :header-rows: 1

   * - 
     - Link
     - Link Access Code
     - Slide Deck
     - Miro Board
   * - End-to-end
     - `Whiteboard 1 <https://VMware.zoom.us/rec/share/Jhdo562s_R6U2Q_TvRNA4G9VWgwZiHV6jFa5bjOn18dO0crYkAVP-NgkXP8wukSU.Wh7JzPNdPJNWrGp_>`_ 
     - pxX*&z7n
     - `Deck <https://onevmw-my.sharepoint.com/:p:/g/personal/jgozzi_vmware_com/EX5ChWsh7QdBovotXGZIbVoBINHBve39PQ506tJJCjc7ig?e=gukNPC>`_
     - `Link <https://miro.com/app/board/o9J_lOpVcNI=/>`_
   * - Data focused
     - `Whiteboard 2 <https://vmware.zoom.us/rec/share/0WyxPjqcBzj92p4j-YuqQQwyk9KN0Hyu41VecoZGGPS8FCj6jPgo3rtaslYH5AYg.DZeZdKCSWqgmL4T0>`_ 
     - F8$58nmf
     - `Deck <https://onevmw-my.sharepoint.com/:p:/g/personal/oawofolu_vmware_com/Ef-7vvj9IA1PpNG33FZTDD0BrQ2mORkloLYgpeYKV8D4IA?e=x97gGc>`_
     - 

.. _decks:

Sample Decks
------------
.. list-table:: 
   :widths: 50 50
   :header-rows: 1

   * - Deck
     - Link
   * - Tanzu Data Overview - High Level Slides
     - `Deck <https://onevmw-my.sharepoint.com/:p:/g/personal/oawofolu_vmware_com/EeC33x74avxPuGghfk5xDooBlJSDffhgf_EVpUsFJAWxjw?e=LcjoXf>`_

.. _demos:

Demos
------
.. list-table:: 
   :widths: 25 25 25 25
   :header-rows: 1

   * - 
     - Link
     - Link Access Code
     - Educates Link
   * - End-to-end
     - `In progress <#>`_
     - 
     - 