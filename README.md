**BUILDING AND DEPLOYING SPHINX SITE**


1. Install Sphinx: see [Installation](https://www.sphinx-doc.org/en/master/usage/installation.html)

2. Clone site directory: 
```
git clone https://gitlab.com/oawofolu/data-enablement-site.git
cd data-enablement-site
```

3. Make changes as appropriate in the *content/* directory (see [docs](https://sublime-and-sphinx-guide.readthedocs.io/en/latest/index.html) for assistance)

4. Build changes: 
```
make html
```
5. Push to TAS - for example, on PCF One:
```
cf push <APP NAME> -p _build/html/ -b staticfile_buildpack
```
